package Menu;
/**
 * 
 * @author Angela Cortecchia
 *
 */
public interface MainMenu {

	/**
	 * 
	 * @param newgame
	 * @return boolean
	 */
	boolean newGame(final boolean newgame);
}
