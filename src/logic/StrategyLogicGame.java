package logic;

import board.BoardPosition;
import player.PlayerColor;

public interface StrategyLogicGame {
	
	
	void changeRounders();//cambia il turbo del giocatore
	
	PlayerColor getRounder();//restituisce il turno del giocatore
	
	boolean checkMate();//controlla se è scacco matto
	
	PlayerColor win();//ritorna chi ha vinto
			
	int getScore(PlayerColor color);// restituisce il punteggio in base alle pedine mangiate
	
	void DoMove(BoardPosition myPosition,BoardPosition nextPosition,boolean checkCanMoveThere);// se ho mosso disabilito le celle degli altri giocatori 
		
}
