package logic;

import java.util.*;

import Sound.Audio;
import board.BoardPosition;
import board.GenericBoard;
import gui.WarningGUI;
import piece.Cell;
import piece.King;
import piece.Pawn;
import piece.Piece;
import piece.Queen;
import piece.Rook;
import player.PlayerColor;
import settings.Translations;
/**
 * Riccardo Foschi
 * 
 * Classe con lo scopo di fare controlli sullo spostamento, in particolare quelli tramite logica di gioco
 *
 */
public class PlayerLogic implements StrategyPlayerLogic{
	
	private LogicGame gameLogic=null;
	
	public PlayerLogic(LogicGame gameLogic)
	{
		this.gameLogic=gameLogic;
	}
	
	/**
	 *  controlla se i tasti premuti e mandati in input sono corretti
	 */
	@Override
	public boolean overBoard(BoardPosition myPosition,BoardPosition nextPosition) {
		if(nextPosition.getX()>=8 || nextPosition.getY()>=8||myPosition.getX()<0||myPosition.getY()<0)
			return false;
		return true;
		
	}
	
	/**
	 *	controlla se il re di un certo colore è attualmente sotto scacco 
	 */	
	@Override
	public boolean chess(ArrayList<ArrayList<Cell>> boardCopy,PlayerColor myColor) {
		BoardPosition king=isKing(boardCopy,myColor);//mi salvo la posizone del re del colore di cui voglio controllare se sono in scacco

		for(ArrayList<Cell>a:boardCopy) {
			
			for(Cell b:a ) {
				if(b.getPiece().isPresent() && b.getPiece().get().getOwner()!=myColor)
				{
					List<BoardPosition> mosse=b.getPiece().get().getPossibleMoves(new GenericBoard(boardCopy));
					for (int i=0; i<mosse.size();i++)
					{
						if (mosse.get(i).getX()==king.getX() && mosse.get(i).getY()==king.getY())
							return true;
					}
				}

			}
		}
		
		return false;// non sono sotto scacco
		
	}

	/**
	 * Controlla se muovo questo pezzo vado in scacco --> non posso muoverlo
	 */
	@Override
	public boolean ifImoveIamInCheck(BoardPosition myPosition) {
		//la tengo  come salvataggio	
		ArrayList<ArrayList<Cell>>secondBoard=gameLogic.chessBoard.getBoardState();
		//metto nella scacchiera il pezzo che vorrei muovere a null in modo tale da verificare se sarei in scacco in sua assenza. 
		searchCell(gameLogic.chessBoard.getBoardState(),myPosition).setPiece(null);
		if(chess(secondBoard,gameLogic.getCurrentPlayerColor())) {
			gameLogic.chessBoard.setBoard(secondBoard);
			return true;
		}
		gameLogic.chessBoard.setBoard(secondBoard);
		
		return false;
	}


	/**
	 * Controlla se una determinata mossa libererebbe un giocatore dallo scacco
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	public boolean check(BoardPosition myPosition,BoardPosition nextPosition,PlayerColor myColor) throws Exception {
		
		ArrayList<ArrayList<Cell>>boardCopy= gameLogic.chessBoard.copyStateByValue();
		MovePiece(boardCopy, myPosition, nextPosition,false);
		boolean a=chess(boardCopy,myColor);
		return a;
	}
	
	/**
	 * Ritorna la posizione del re del colore che voglio 
	 */
	public BoardPosition isKing(ArrayList<ArrayList<Cell>> boardCopy,PlayerColor myPlayerColor) {
		for(ArrayList<Cell>a:boardCopy) {
			
			for(Cell b:a ) {
				if(b.getPiece().isPresent() && b.getPiece().get().getOwner()==myPlayerColor){
					if(b.getPiece().get() instanceof King) {
						return b.getBoardPosition();
					}
				}

			}
		}
		return null;//se mi ritorna null vuole dire che è sbagliato perchè impossibile che non mi trovi il re.
		
		
	}
	
	/**
	 * Controlla se sta muovendo un mio pezzo
	 */
	@Override
	public boolean myTurn(Cell a) {
		if(gameLogic.getCurrentPlayerColor()==a.getPiece().get().getOwner())
			return true;
		return false;
	}
	 

	
	 /**
	  * Ritorna se è occupato da un mio pezzo
	  */
	 private boolean isOccupiedByMe(PlayerColor side,Cell cellToCheck) {
		 if(cellToCheck.getPiece().isPresent() && cellToCheck.getPiece().get().getOwner()==side)
			 return true;
		 return false;
					 
	 }
		
	 /**
	  * mi ritorna la cella di quella determinata posizione
	  */
	 public Cell searchCell(ArrayList<ArrayList<Cell>>myBoard,BoardPosition position) {
		 return myBoard.get(position.getX()).get(position.getY());		
	 }

		
	 /**
	  * Stampa in console una versione a caratteri della scacchiera, molto utile per testing
	  * 
	  */		 
	 @SuppressWarnings("unused")
	private void printBoard (ArrayList<ArrayList<Cell>> newChessBoard)
	 {
		 for (int i=0; i<8; i++)
		 {
			 String s="";
			 for (int j=0; j<8; j++)
			 {
				 if (newChessBoard.get(i).get(j).getPiece().isPresent())
						 s+=newChessBoard.get(i).get(j).getPiece().get();
				 else
					 s+="X";
				 s+="\t";
			 }
			 System.out.println(s);
		 }
		 System.out.println(newChessBoard.toString());
	 }
	 
	 /**
	  * Movimento  
	  */
	 public boolean move(BoardPosition myPosition, BoardPosition nextPosition, boolean checkCanMoveThere) throws Exception {
		 ArrayList<ArrayList<Cell>> newChessBoard=gameLogic.chessBoard.getBoardState();//utilizzo una chessboard secondaria ogni volta come supporot
		 
		 //controllo se sono accettabili i valori presi
		 if(checkCanMoveThere && !overBoard(myPosition,nextPosition)) {
			 new WarningGUI(Translations.getTranslation("cantmovethere"));
			 return false;
		 }
		 
		 //non ha selezionato una pedina da spostare
		 if(!searchCell(newChessBoard,myPosition).getPiece().isPresent()) {
			 new WarningGUI("Nessuna pedina selezionata!");
			 return false;
			 
		 }
		 //controllo se Ã¨ il mio turno
		 if(!myTurn(searchCell(newChessBoard,myPosition))) {
			 	new WarningGUI("Attendi il tuo turno!");
			  return false;
		  }		 

		 Cell selectedCell=searchCell(newChessBoard,myPosition);
		 
		 if (checkCanMoveThere)
		 {
			 List<BoardPosition> possibleMoves=selectedCell.getPiece().get().getPossibleMoves(gameLogic.chessBoard);
			 boolean isInsideList=false;
			 for (int i =0; i<possibleMoves.size();i++)
			 {
				 if (possibleMoves.get(i).getX()==nextPosition.getX() && possibleMoves.get(i).getY()==nextPosition.getY())
				 {
					 isInsideList=true;
					 break;
				 }
			 }
			 if(!isInsideList) {
				 new WarningGUI(Translations.getTranslation("cantmovethere"));
				 return false;
			 }
		 }
		 
		 //check -> Dopo aver fatto quella mossa, sarei ancora in scacco?
		 if(checkCanMoveThere &&  check(myPosition,nextPosition,searchCell(newChessBoard,myPosition).getPiece().get().getOwner())) {
			 	new WarningGUI("Dopo questa mossa, il re sarebbe in scacco!");
			 
			 return false;
		 }
		 //controllo se non è ccupato da un mio pezzo

			
		 if(!isOccupiedByMe(selectedCell.getPiece().get().getOwner(),searchCell(newChessBoard,nextPosition))) {
			 registerEatAt(newChessBoard,nextPosition);
			 //Fixato spostamento effettivo
		

			 Piece pezzodaMuovere = gameLogic.chessBoard.getPieceAt(myPosition).get();
			 if(pezzodaMuovere instanceof King )
			 {
				 if (Math.abs(nextPosition.getY()-myPosition.getY())>1)
				 {
					 //STA FACENDO UN ARROCCO
					 if (nextPosition.getY()>4)
					 {
						 BoardPosition towerFrom=new BoardPosition(nextPosition.getX(),7);
						 BoardPosition towerTo=new BoardPosition(nextPosition.getX(),5);
						 MovePiece(newChessBoard,towerFrom,towerTo,false);
					 }
					 else
					 {
						 BoardPosition towerFrom=new BoardPosition(nextPosition.getX(),0);
						 BoardPosition towerTo=new BoardPosition(nextPosition.getX(),3);
						 MovePiece(newChessBoard,towerFrom,towerTo,false);
					 }
						 
				 }
			 }
			 
			 //campio posizione del pezzo --> movimento effettivo
			 MovePiece(newChessBoard,myPosition,nextPosition,true);
			 
			 Piece pezzoMosso = gameLogic.chessBoard.getPieceAt(nextPosition).get();
			 if (pezzoMosso instanceof Rook)
				 ((Rook) pezzoMosso).markIsFirst();
			 if(pezzoMosso instanceof King )
				 ((King) pezzoMosso).markIsFirst();
			 

			 return true; //ho fatto lo spostamento
			 
		 }
		 return false;//non posso fare lo spostamento		
		 
	 }
	 
	 /**
	  * Registra un pedino ad una determinata cella come mangiato
	  */
	 void registerEatAt(ArrayList<ArrayList<Cell>> board,BoardPosition position)
	 {
		 //se Ã¨ presente un pezzo dell avversario lo mangio
		 if(searchCell(board,position).getPiece().isPresent()) {
			 //inserisco il pezzo mangiato nella mia lista di pezzi mangiati
			 gameLogic.eat.add(searchCell(board,position).getPiece().get());
			 //se voglio eliminare il pezzo
			 searchCell(board,position).setPiece(null);						 							 
		 }
	 }
	 /**
	  * Muove il pezzo ed aggiorna la gui
	  */
	 void MovePiece(ArrayList<ArrayList<Cell>> boardState,BoardPosition from, BoardPosition to, boolean updateGUI) {
		 searchCell(boardState,to).setPiece(searchCell(boardState,from).getPiece());
		 searchCell(boardState,to).getPiece().get().setPosition(to);
		 searchCell(boardState,from).setPiece(Optional.empty());
		 //usedChessboard.setBoard(boardState);
		 if (updateGUI && gameLogic.matchManager!=null && gameLogic.matchManager.getCurrentBoardGUI()!=null){
			 gameLogic.matchManager.getCurrentBoardGUI().UpdateGUI();
				Audio.playSound("/clips/move_01.wav");
		 }
		 //trasforma pedini in regine
		 Piece p = searchCell(boardState,to).getPiece().get();
		 if (p instanceof Pawn && (p.getOwner()==PlayerColor.WHITE && p.getPosition().getX()==7 || p.getOwner()==PlayerColor.BLACK && p.getPosition().getX()==0 )){
			 searchCell(boardState,to).setPiece(Optional.of(new Queen(p.getOwner(),to)));
			 searchCell(boardState,to).getPiece().get().setPosition(to);
		 }
	 }

}