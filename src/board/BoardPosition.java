package board;


/**
 * a simple class used to ease the management
 * of pieces and cells
 */
public class BoardPosition {

	private int x;
	private int y;
	
	/**
	 * a constuctor for machines
	 * @param x from 0 to 7
	 * @param y from 0 to 7
	 */
	public BoardPosition(final int x, final int y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * a constructor for humans
	 * @param column from 'a' to 'h'
	 * @param row from 1 to 8
	 */
	public BoardPosition (String column, Integer row) {
		this.y = Character.valueOf(column.charAt(0)) - 'a';	
		this.x = row.intValue() - 1;
	}
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean isValidClassicBoardPositions() {
		return this.x >= 0 && this.x <= 7 && this.y >= 0 && this.y <= 7;
	}
	
	public String toString() {
		
		char column =  (char) ('a' + this.y);
		char row = (char) ('1' + this.x);

		return String.valueOf(column) + String.valueOf(row);
	}
}
