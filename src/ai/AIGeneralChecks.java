package ai;

import static org.junit.jupiter.api.Assertions.*;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import ai.StockfishClient;
import ai.enums.*;
import ai.exceptions.StockfishInitException;

class AIGeneralChecks {

	/**
	 * this is done only for checking that
	 * the simplest situation gives obvious results
	 * 
	 * @throws InterruptedException
	 * @throws StockfishInitException
	 */
	@Test
	void testInitialPosition() throws InterruptedException, StockfishInitException {
		StockfishClient client = new StockfishClient.Builder()
        		.setPath(getClass().getResource("/ai/engine").getFile())
		        //.setPath("src\\ai\\engine\\")//questo spacca tutto per me
		        .setOption(Option.Skill_Level, 3) // Stockfish skill level 0-20
		        .setVariant(Variant.BMI2)
		        .build();
		
		Query query = new Query.Builder(QueryType.Legal_Moves)
		        .setFen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
		        .setDepth(12)
		        .build();
						
		String [] unPassoIPedoni = {"a2a3","b2b3","c2c3","d2d3","e2e3","f2f3","g2g3","h2h3"};
		String [] duePassiIPedoni = {"a2a4","b2b4","c2c4","d2d4","e2e4","f2f4","g2g4","h2h4"};
		String [] iCavalli = {"b1a3","b1c3","g1f3","g1h3"};

		String legalMoves = client.thinkOfAMove(query);
		String[] mosseRisultanti = legalMoves.split(" ");
		
		List<String> mosseDaControllare = new ArrayList<String>();
		mosseDaControllare.addAll(Arrays.asList(unPassoIPedoni));
		mosseDaControllare.addAll(Arrays.asList(duePassiIPedoni));
		mosseDaControllare.addAll(Arrays.asList(iCavalli));
		
		assertEquals(mosseDaControllare.size(), mosseRisultanti.length);
		
		for(int i = 0; i<mosseRisultanti.length; i++)
			assertTrue(mosseDaControllare.contains(mosseRisultanti[i]));

	}
	
	@Test
	void testConversionToPosition() throws InterruptedException, StockfishInitException {
		StockfishClient client = new StockfishClient.Builder()
		        .setPath("src\\ai\\engine\\")
		        .setOption(Option.Skill_Level, 3) // Stockfish skill level 0-20
		        .setVariant(Variant.BMI2)
		        .build();
		
		Query query = new Query.Builder(QueryType.Best_Move)
		        .setFen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
		        .setDepth(12)
		        .build();
		
		Move move = StockfishClient.convertStringToMove(client.thinkOfAMove(query));
		System.out.println(move);
		
	}

	@Test
	void testThinkingFrame() throws MalformedURLException, InterruptedException {
		AIThinkingFrame aitf = new AIThinkingFrame();
		aitf.showDelayAndDestroyARandomGif(3);
	}

}
