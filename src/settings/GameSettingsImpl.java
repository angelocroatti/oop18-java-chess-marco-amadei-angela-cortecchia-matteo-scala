package settings;

/**
 * 
 * @author Angela Cortecchia
 *
 */

public class GameSettingsImpl implements GameSettings {

	public Difficulty diff;
	public GameMode m;
	public BoardColors bColor;
	public Players players;
	public GameTime time;
	public PiecesSkin skin;

	public GameMode setGameMode() {
			return this.m;
	}
	
	public Difficulty setDifficulty() {
		return this.diff;
	}
	
	public BoardColors setColor() {
		return this.bColor;
	}

	public Players setPlayers() {
		return this.players;
	}

	public GameTime setGameTime() {
		return this.time;
	}

	public PiecesSkin setPieceSkin() {
		return this.skin;
	}
}
