package settings;
/**
 * 
 * @author Angela Cortecchia
 *
 */
public interface GlobalSettings {

	enum CurrentLanguage{
		ITALIAN,
		ENGLISH,
		GERMAN,
		SPANISH,
		FRENCH;
	}
/**
 * 	
 * @return CurrentLanguage chosen from settings in GUI
 */
	CurrentLanguage setLanguage();
	
	enum Difficulty{
		EASY,
		MEDIUM,
		HARD;
	}
	
	/**
	 * 
	 * @return Difficulty chosen from settings in GUI
	 */
	Difficulty setDifficulty();
	
	/**
	 * 
	 * @param enable
	 * @return true if hints are enabled
	 */
	boolean hintsEnabled(boolean enable);
	
	enum PieceSet{ //customizza le pedine in base al set scelto??
		BISHOP,
		KNIGHT,
		KING,
		QUEEN,
		ROOK,
		PAWN;
	}
	
	/**
	 * 
	 * @param username
	 * @return a String with the name of the player
	 */
	String setPlayerName(String username);
	
	enum Mode{
		ONEVSONE,
		FOURPLAYERS
	}
	
	/**
	 * 
	 * @return the mode of the game
	 */
	Mode setGameMode();
	
	enum Colors{
		WHITE,
		BLACK,
		BLUE,
		YELLOW,
		ORANGE,
		RED,
		LIGHTBLUE,
		PINK;
	}
	
	/**
	 * 
	 * @return the color chosen
	 */
	Colors setColor();
	
	/**
	 * 
	 * @param enable
	 * @return true if the user wants to see the history
	 */
	boolean showHistory(boolean enable);

	/**
	 * 
	 * @param enable
	 * @return true if the user chose to keep sounds effects during the game
	 */
	boolean soundsEnabled(boolean enable);
}