package settings;
/**
 * 
 * @author Angela Cortecchia
 *
 */
public class GlobalSettingsImpl implements GlobalSettings {

	public CurrentLanguage lang;
	public Mode m;
	public Difficulty diff;
	public Colors color;
	public String name;
	public int width;
	public int height;
	public static boolean isHintEnabled = true;
	public static boolean isAudioEnabled = true;
	//public static boolean isTimeEnabled = true;
	public boolean showHis = false;
	
	public static GlobalSettingsImpl globalSettings = new GlobalSettingsImpl();
	
	/**
	 * 	
	 * @return CurrentLanguage chosen from settings in GUI
	 */
	public CurrentLanguage setLanguage() {
		return this.lang;
	}
	
	/**
	 * 
	 * @return the mode of the game
	 */
		public Mode setGameMode() {
			return this.m;
	}
	
	/**
	 * 
	 * @return Difficulty chosen from settings in GUI
	 */
	public Difficulty setDifficulty() {
		return this.diff;
	}
	
	/**
	 * 
	 * @return the color chosen
	 */
	public Colors setColor() {
		return this.color;
	}
	
	/**
	 * 
	 * @param enable
	 * @return true if hints are enabled
	 */
	
	public boolean hintsEnabled(boolean enable) {
		return isHintEnabled = enable;
	}

	/**
	 * 
	 * @param enable
	 * @return true if the user chose to keep sounds effects during the game
	 */
	public boolean soundsEnabled(boolean enable) {
		return isAudioEnabled = enable;
	}

	/**
	 * 
	 * @param username
	 * @return a String with the name of the player
	 */
	public String setPlayerName(final String username) {
		return this.name = username;
	}
	
	/**
	 * 
	 * @param enable
	 * @return true if the user wants to see the history
	 */
	public boolean showHistory(boolean enable) {
		return this.showHis = enable;	
	}
}