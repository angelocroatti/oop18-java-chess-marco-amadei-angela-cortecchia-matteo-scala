package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

//import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import board.BoardPosition;
import board.BoardSchema;
import board.GenericBoard;
import logic.MatchManager;
import logic.PlayerLogic;
import logic.PlayerType;
import player.PlayerColor;
import settings.MatchSettings.Difficulty;
import settings.MatchSettings.GameMode;
import settings.MatchSettings.GameTime;
/**
 * 
 * Foschi Riccardo
 * Vari metodi pre-fissati
 * Scopo -> test
 */
class TestLogic {

	
	String chessMateBoard = ""
			+ "R1------K0------/"
			+ "--Q1------------/"
			+ "----------------/"
			+ "----------------/"
			+ "----------------/"
			+ "----------------/"
			+ "----------------/"
			+ "--------K1------";
	
	String chessBoard = ""
			+ "------Q0K0----Q1/"
			+ "----------------/"
			+ "----------------/"
			+ "----------------/"
			+ "----------------/"
			+ "----------------/"
			+ "----------------/"
			+ "--------K1------";
	
	String normalBoard = ""
			+ "--------K0------/"
			+ "----------------/"
			+ "----------------/"
			+ "----------------/"
			+ "--N1------------/"
			+ "----------------/"
			+ "----------------/"
			+ "--------K1------";

	/**
	 * test per generare board da zero
	 * @throws Exception
	 */
	@Test
	void TestCreateBoard() throws Exception {
		new GenericBoard(BoardSchema.buildBoard(chessMateBoard));
		new GenericBoard(BoardSchema.buildBoard(chessBoard));
		new GenericBoard(BoardSchema.buildBoard(normalBoard));
	}
	/**
	 * 
	 * test per verificare chessMate
	 */
	@Test
	void TestChessMate() throws Exception {
		GenericBoard gb =  new GenericBoard(BoardSchema.buildBoard(chessMateBoard));
		
		PlayerType [] players = new PlayerType[2];
		players[0]=new PlayerType(false,"player 1");
		players[1]=new PlayerType(false,"player 2");
		try {
			MatchManager mm =new MatchManager(Difficulty.NORMAL,GameMode.STANDARD,"standard",GameTime.disabled,players,1);
			mm.getGameLogic().setChessBoard(gb);
			
			assertTrue(mm.getGameLogic().checkMate());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	/**
	 * test per verificare che ci sia scacco
	 * @throws Exception
	 */
	@Test
	void TestIsChess() throws Exception {
		assertTrue(new PlayerLogic(null).chess(BoardSchema.buildBoard(chessBoard), PlayerColor.WHITE));
	}
	/**
	 * 
	 * test per verificar eche non ci sia  scacco
	 * @throws Exception
	 */
	@Test
	void TestIsNotChess() throws Exception{
		assertFalse(new PlayerLogic(null).chess(BoardSchema.buildBoard(normalBoard), PlayerColor.WHITE));
		assertTrue(new PlayerLogic(null).chess(BoardSchema.buildBoard(chessBoard), PlayerColor.WHITE));
	}
	
	/**
	 * 
	 * test per veirficare che il pezzo sia realmente spostato
	 * @throws Exception
	 */
	@Test
	void TestMove() throws Exception{
		
		GenericBoard gb =  new GenericBoard(BoardSchema.buildBoard(normalBoard));
		
		PlayerType [] players = new PlayerType[2];
		players[0]=new PlayerType(false,"player 1");
		players[1]=new PlayerType(false,"player 2");
		try {
			MatchManager mm =new MatchManager(Difficulty.NORMAL,GameMode.STANDARD,"standard",GameTime.disabled,players,1);
			mm.getGameLogic().setChessBoard(gb);
			
			BoardPosition starterPosition=new BoardPosition(0,4);
			BoardPosition destination=new BoardPosition(1,3);
			mm.getGameLogic().DoMove(starterPosition, destination, true);
			assertTrue(mm.getGameLogic().chessBoard.getPieceAt(destination).isPresent());
			assertFalse(mm.getGameLogic().chessBoard.getPieceAt(starterPosition).isPresent());
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		
		
	}

}
