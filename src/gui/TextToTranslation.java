package gui;
/**
 * 
 * @author Marco Amadei
 * 
 * Classe per mantenere aggiornata una traduzione in caso di cambio di lingua
 *
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import settings.Translations;

public abstract class TextToTranslation 
{
	public static List<TextToTranslation> buttonTotranslate=new ArrayList<TextToTranslation>();
	
	private String translation_method="";

    /**
     * Set and self-update translation from its ID.
     * @param translation_id ID of the needed translation.
     */
	public void setTranslation(String translation_id)
	{
		this.translation_method=translation_id;
		updateTranslation();
		buttonTotranslate.add(this);
	}

    /**
     * Update this translation.
     */
	public void updateTranslation() 
	{
		System.out.print("ESTENDIMI");
	}

    /**
     * Update all active texts.
     */
	public static void updateAllTranslations()
	{
		buttonTotranslate.removeAll(Collections.singleton(null));//remove all null (if buttton has been reoved)
		for (int i=0; i<buttonTotranslate.size(); i++)
			if (buttonTotranslate.get(i)!=null)
				buttonTotranslate.get(i).updateTranslation();
	}

    /**
     * Get translation from its translationID.
     * @return Updated translation for this text component.
     */
	public String getTranslation()
	{
		return (String) Translations.getTranslation(translation_method);
	}
}
