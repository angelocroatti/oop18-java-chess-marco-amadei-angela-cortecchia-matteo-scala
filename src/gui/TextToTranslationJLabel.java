package gui;
/**
 * 
 * @author Marco Amadei
 * 
 * Classe per mantenere aggiornata una traduzione in caso di cambio di lingua (per i JLabel)
 *
 */
import javax.swing.JLabel;

public class TextToTranslationJLabel extends TextToTranslation {
	
	private JLabel label;

	public TextToTranslationJLabel(JLabel label, String translation_id)
	{
		this.label=label;
		setTranslation(translation_id);
	}

	@Override
	public void updateTranslation() 
	{
		label.setText(getTranslation());
	}

}
