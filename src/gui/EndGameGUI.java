package gui;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import Sound.Audio;

import java.awt.Color;

import javax.swing.Box;


import ai.ColumnLayout;
import settings.Translations;

public class EndGameGUI extends JFrame{

	private static final long serialVersionUID = 1L;

	private JPanel mainPanel = new JPanel(new ColumnLayout());
	private JTextArea playerName;
	private JTextArea playerScore;
	private JButton nextOperationButton;
	
	/**
	 * 
	 * @param winnerPlayerName
	 * @param winnerPlayerScore
	 * @param nextPhase true if you want to propose other chess games, false if gonna close
	 * @param gui the current chessboard gui
	 */
	public EndGameGUI(String winnerPlayerName, int winnerPlayerScore, boolean nextPhase ,BoardGUI gui) {
		
		String winnerTradotto= Translations.getTranslation("Winner");
		playerName = new JTextArea(winnerTradotto+ ": " + winnerPlayerName);
		String scoreTradotto = Translations.getTranslation("score");
		playerScore = new JTextArea(scoreTradotto+ ": " + String.valueOf(winnerPlayerScore));
		
		nextOperationButton = new JButton(nextPhase? Translations.getTranslation("continuegame") : Translations.getTranslation("close") );
		nextOperationButton.addActionListener( evt -> {
			Audio.playSound("/clips/button_01.wav");
			gui.OnEndGameGUIClosed();
	    	setVisible(false);
	    	Thread.currentThread().interrupt();
		} );
		
		mainPanel.add(new Box(1));
		mainPanel.add(playerName);
		mainPanel.add(new Box(2));
		mainPanel.add(playerScore);
		mainPanel.add(new Box(3));
		mainPanel.add(nextOperationButton);

		mainPanel.setOpaque(true);
		mainPanel.setBackground(Color.GRAY);
		mainPanel.setForeground(Color.BLACK);

		
		this.add(mainPanel);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(150, 200);
	}
	
	

}
