package piece;

import java.util.ArrayList;
import java.util.List;

import board.BoardPosition;
import board.GenericBoard;
import player.PlayerColor;

public class Pawn extends AbstractPiece{

	public Pawn() {
		super();
	}
	
	public Pawn(PlayerColor owner, BoardPosition position) {
		super(owner, position);
	}

	@Override
	public List<BoardPosition> getPossibleMoves(GenericBoard board) {
		List<BoardPosition> possibleMoves = new ArrayList<>();
		if(this.owner.equals(PlayerColor.BLACK))
		{
			if(new BoardPosition(position.getX()-1, position.getY()).isValidClassicBoardPositions() && !board.getPieceColorAtPosition(new BoardPosition(position.getX()-1, position.getY())).isPresent())
			{
				possibleMoves.add(new BoardPosition(position.getX()-1, position.getY()));
				if(this.position.getX() == 6 && !board.getPieceAt(new BoardPosition(position.getX()-2, position.getY())).isPresent())
					possibleMoves.add(new BoardPosition(position.getX()-2, position.getY()));
			}
			if(new BoardPosition(position.getX()-1, position.getY()+1).isValidClassicBoardPositions() && board.getPieceColorAtPosition(new BoardPosition(position.getX()-1, position.getY()+1)).isPresent()) {
				if(board.getPieceColorAtPosition(new BoardPosition(position.getX()-1, position.getY()+1)).get().equals(PlayerColor.WHITE))
						possibleMoves.add(new BoardPosition(position.getX()-1, position.getY()+1));
			}
			if(new BoardPosition(position.getX()-1, position.getY()-1).isValidClassicBoardPositions() && board.getPieceColorAtPosition(new BoardPosition(position.getX()-1, position.getY()-1)).isPresent()) {
				if(board.getPieceColorAtPosition(new BoardPosition(position.getX()-1, position.getY()-1)).get().equals(PlayerColor.WHITE))
						possibleMoves.add(new BoardPosition(position.getX()-1, position.getY()-1));
			}
		}
		else
		{
			if(new BoardPosition(position.getX()+1, position.getY()).isValidClassicBoardPositions() && !board.getPieceColorAtPosition(new BoardPosition(position.getX()+1, position.getY())).isPresent())
			{
				possibleMoves.add(new BoardPosition(position.getX()+1, position.getY()));
				if(this.position.getX() == 1 && !board.getPieceAt(new BoardPosition(position.getX()+2, position.getY())).isPresent())
					possibleMoves.add(new BoardPosition(position.getX()+2, position.getY()));
			}
			if(new BoardPosition(position.getX()+1, position.getY()+1).isValidClassicBoardPositions() && board.getPieceColorAtPosition(new BoardPosition(position.getX()+1, position.getY()+1)).isPresent()) {
					if(board.getPieceColorAtPosition(new BoardPosition(position.getX()+1, position.getY()+1)).get().equals(PlayerColor.BLACK))
							possibleMoves.add(new BoardPosition(position.getX()+1, position.getY()+1));
			}
			if(new BoardPosition(position.getX()+1, position.getY()-1).isValidClassicBoardPositions() && board.getPieceColorAtPosition(new BoardPosition(position.getX()+1, position.getY()-1)).isPresent()) {
					if(board.getPieceColorAtPosition(new BoardPosition(position.getX()+1, position.getY()-1)).get().equals(PlayerColor.BLACK))
				possibleMoves.add(new BoardPosition(position.getX()+1, position.getY()-1));
			}
		}
		
		return possibleMoves;
	}

	@Override
	public String getIcon() {
		return "Pawn.png";
	}

	@Override
	public int getScore() {
		return 1;
	}
	
	@Override
	public String toString() {
		return "p";
	} 
}
