package piece;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import board.BoardPosition;
import board.GenericBoard;
import player.PlayerColor;

public class Knight extends AbstractPiece{

	public Knight() {
		super();
	}
	
	public Knight(PlayerColor owner, BoardPosition position) {
		super(owner, position);
	}

	@Override
	public List<BoardPosition> getPossibleMoves(GenericBoard board) {
		List<BoardPosition> possibleMoves = new ArrayList<>();
		possibleMoves.add(new BoardPosition(position.getX()-2, position.getY()-1));
		possibleMoves.add(new BoardPosition(position.getX()-2, position.getY()+1));
		possibleMoves.add(new BoardPosition(position.getX()+2, position.getY()-1));
		possibleMoves.add(new BoardPosition(position.getX()+2, position.getY()+1));
		possibleMoves.add(new BoardPosition(position.getX()-1, position.getY()-2));
		possibleMoves.add(new BoardPosition(position.getX()+1, position.getY()-2));
		possibleMoves.add(new BoardPosition(position.getX()-1, position.getY()+2));
		possibleMoves.add(new BoardPosition(position.getX()+1, position.getY()+2));
		possibleMoves = possibleMoves.stream().filter(x -> x.isValidClassicBoardPositions())
							  .filter(x -> !board.getPieceColorAtPosition(x).isPresent() || !board.getPieceColorAtPosition(x).get().equals(this.owner))
							  .collect(Collectors.toList());
		return possibleMoves;
	}

	@Override
	public String getIcon() {
		return "Knight.png";
	}

	@Override
	public int getScore() {
		return 3;
	}
	
	@Override
	public String toString() {
		return "n";
	}
}
