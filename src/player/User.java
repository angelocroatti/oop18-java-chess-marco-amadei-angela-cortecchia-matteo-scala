package player;
/**
 * 
 * @author Angela Cortecchia
 *
 */
import settings.MatchSettings;

public interface User {

	/**
	 * 
	 * @return a string with the name of the user
	 */
	String getUser();
	
	/**
	 * 
	 * @return the difficult chosen
	 */
	MatchSettings.Difficulty getDifficulty();
	
	enum ColorPlayer{
		WHITE,
		BLACK;
	}
	
	/**
	 * 
	 * @return the color chosen
	 */
	ColorPlayer getColor();
}
