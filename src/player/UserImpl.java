package player;

import settings.MatchSettings.Difficulty;
/**
 * 
 * @author Angela Cortecchia
 *
 */
public class UserImpl implements User {
	 
	public String username;
	public static Difficulty difficulty;
	public ColorPlayer color;
	
	/**
	 * constructor
	 * @param name
	 * @param diff
	 * @param color
	 */
	public UserImpl(final String name, final Difficulty diff, final ColorPlayer color) {
		this.username = name;
		UserImpl.difficulty = diff;
		this.color = color;
	}
	
	/**
	 * 
	 * @return a string with the name of the user
	 */
	public String getUser() {
		return this.username;
	}

	/**
	 * 
	 * @return the difficult chosen
	 */
	public Difficulty getDifficulty() {
		return UserImpl.difficulty;
	}
	 
	/**
	 * 
	 * @return the color chosen
	 */
	public ColorPlayer getColor() {
		return this.color;
	}
}
